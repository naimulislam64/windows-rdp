# Use the base image
FROM dorowu/ubuntu-desktop-lxde-vnc

# Expose the port on which NoVNC runs (80 inside the container)
EXPOSE 80

# Set the environment variable for screen resolution
ENV RESOLUTION 1240x620

# Start the command to run NOVNC
CMD ["supervisord", "c", "/etc/supervisor/supervisord.conf"]